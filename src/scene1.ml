open Exp

module S : SCENE  = struct
  let name = "lwt-race"

  let default_params = [| "4"; "7"; "never" |]
  let decode_params params =
    Array.map
      (fun s -> if s = "never" then Delay.Never else Delay.Pauses (int_of_string s))
      params

  let f params =
    let params = decode_params (if params = [||] then default_params else params) in
    Lwt_eio.Promise.await_lwt begin
      Lwt.pick (
        List.mapi
          (fun i d -> lwt_tracing_promise ("p" ^ string_of_int i) d)
          @@ Array.to_list params
      )
    end
end
