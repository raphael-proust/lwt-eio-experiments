open Exp

let run (module S : SCENE) params =
  traceln ~__LOC__ "scene(%s) start: %a"
    S.name
    (Format.pp_print_seq ~pp_sep:Format.pp_print_space Format.pp_print_string) (Array.to_seq params);
  match S.f params with
  | Ok () -> traceln ~__LOC__ "scene(%s) end" S.name
  | Error exc -> traceln ~__LOC__ "scene(%s) error: %s" S.name (Printexc.to_string exc)
  | exception exc -> traceln ~__LOC__ "scene(%s) crash: %s" S.name (Printexc.to_string exc)

let scenes : (module SCENE) list = [
  (module Scene1.S);
  (module Scene2.S);
  (module Scene3.S);
]

let () =
  if Array.length Sys.argv = 1 || Sys.argv.(1) = "list" then begin
    List.iter
      (fun (module S : SCENE) -> print_endline S.name)
      scenes;
    exit 0
  end

let scene =
  let n = Sys.argv.(1) in
  List.find (fun (module S : SCENE) -> S.name = n) scenes

let params =
  Array.sub Sys.argv 2 (Array.length Sys.argv - 2)

let () =
  Eio_main.run @@ fun env ->
    Lwt_eio.with_event_loop ~clock:env#clock @@ fun _ ->
      run scene params
