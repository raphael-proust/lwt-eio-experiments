open Exp

(* NOTES:

   In this experiment we can actually witness that in a mixed Eio-Lwt context
   the `on_cancel` hook of Lwt promises is not called.

   Not only that, if you provide multiple Lwt promises that finish earlier than
   any of the provided Eio promises (e.g., `L1 L2 L2 L3 E10`) then all Lwt
   promises are given a chance to finish: they are not canceled.

   This shows that the canceling from the `Eio.Fiber.any` does not propagate
   into the calls to `Lwt_eio.Promise.await_lwt`.
 *)

module S : SCENE  = struct
  let name = "lwt-eio-race"

  let def_params = [| "L7"; "L9"; "Enever"; "E1" |]

  let decode_param param =
    let system =
      match param.[0] with
      | 'E' -> `Eio
      | 'L' -> `Lwt
      | _ -> raise (Invalid_argument "decode_param")
    in
    let delay = String.sub param 1 (String.length param - 1) in
    let delay = if delay = "never" then Delay.Never else Delay.Pauses (int_of_string delay) in
    (system, delay)

  let decode_params params = List.map decode_param @@ Array.to_list params

  let f params =
    let params = if params = [||] then def_params else params in
    Eio.Fiber.any (
      List.mapi
        (fun i (s, d) ->
          match s with
          | `Lwt ->
              (fun () -> Lwt_eio.Promise.await_lwt (lwt_tracing_promise ("lwt" ^ string_of_int i) d))
          | `Eio ->
              (fun () -> (eio_tracing_promise ("eio" ^ string_of_int i) d))
        )
        (decode_params params)
    )
end
