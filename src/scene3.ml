open Exp

module S : SCENE = struct
  let name = "resolves"
  let f _params =
    (* cross framework promise resolution *)
    let lwt_promise, lwt_resolver = Lwt.task () in
    let eio_promise, eio_resolver = Eio.Promise.create () in
    Eio.Fiber.first
      (fun () -> Lwt_eio.Promise.await_lwt (
        let open Lwt.Syntax in
        let* () = Delay.lwt (Pauses 3) in
        traceln ~__LOC__ "LWT sends 'hello' to EIO";
        let () = Eio.Promise.resolve eio_resolver "hello" in
        let* () = Delay.lwt (Pauses 2) in
        let* x = lwt_promise in
        assert (x = "world");
        traceln ~__LOC__ "LWT got 'world' from EIO";
        Lwt.return_unit
      ))
      (fun () ->
        Delay.eio (Pauses 1);
        let x = Eio.Promise.await eio_promise in
        assert (x = "hello");
        traceln ~__LOC__ "EIO got 'hello' from LWT";
        Delay.eio (Pauses 5);
        traceln ~__LOC__ "EIO sends 'world' to LWT";
        Lwt.wakeup lwt_resolver "world";
        ()
      );
    Ok ()
end
