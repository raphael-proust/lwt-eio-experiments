let traceln ~__LOC__ =
  Format.kasprintf
    (fun s -> Format.printf "%s: %s\n" __LOC__ s)

module Delay
: sig
  type delay =
    | Sleep of float
    | Pauses of int
    | Never
  val lwt : delay -> unit Lwt.t
  val eio : delay -> unit
end
= struct

  type delay =
    | Sleep of float
    | Pauses of int
    | Never

  let lwt =
    let open Lwt.Syntax in
    function
    | Sleep f ->
      let* () = Lwt_unix.sleep f in
      Lwt.return_unit
    | Pauses n ->
      let rec pause_n n =
        if n <= 0 then Lwt.return_unit else let* () = Lwt.pause () in pause_n (n - 1)
      in
      let* () = pause_n n in
      Lwt.return_unit
    | Never ->
      fst (Lwt.task ())

  let eio = function
    | Sleep f -> Eio_unix.sleep f
    | Pauses n -> for _ = 1 to n do Eio.Fiber.yield () done
    | Never -> Eio.Fiber.any []

end


let lwt_tracing_promise s d =
  let open Lwt.Syntax in
  let () = traceln ~__LOC__ "LWT promise %s: DELAY" s in
  let* () = Delay.lwt d in
  let () = traceln ~__LOC__ "LWT promise %s: DELAYED" s in
  Lwt.return_unit

let lwt_tracing_promise s d =
  let p = lwt_tracing_promise s d in
  (* this is printed when a promise is canceled, currently it only works when
     we're racing Lwt promises only (scenario 1) but not when we are racing Lwt
     and Eio promises (scenario 2) *)
  Lwt.on_cancel p (fun () -> traceln ~__LOC__ "LWT promise %s: CANCELED" s);
  Lwt_result.catch p

let eio_tracing_promise s d =
  let () = traceln ~__LOC__ "EIO promise %s: DELAY" s in
  let () = Delay.eio d in
  let () = traceln ~__LOC__ "EIO promise %s: DELAYED" s in
  ()

let eio_tracing_promise s d =
  match
    (* no (?) way to attach an on-cancel hook *)
    eio_tracing_promise s d
  with
  | v -> Ok v
  | exception exc -> Error exc


module type SCENE = sig
  val f : string array -> (unit, exn) result
  val name : string
end
